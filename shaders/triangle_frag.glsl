#version 330 core

uniform sampler2D TextureSampler;
uniform sampler2D CloudsTexture;

uniform float Time;

uniform vec2 CloudsRotationSpeed = vec2(0.01, 0.009);

in vec3 Normal;
in vec3 Color;
in vec2 UV;

uniform vec3 LightDirection;
uniform float LightIntensity;

out vec4 OutColor;

void main() {

	// Renormalizar a normal para evitar problemas com a interpolação linear
	vec3 N = normalize(Normal);

	// Inverter a direção da luz para calcular o vetor L utilizado no modelo de iluminação
	vec3 L = -normalize(LightDirection);

	float Lambertian = max(dot(N, L), 0.0);
	Lambertian = clamp(Lambertian, 0.0, 1.0);

	float SpecularReflection = 0.0;

	if (Lambertian > 0.0) {
		// Vetor V
		vec3 ViewDirection = vec3(0.0, 0.0, -1.0);
		// Vetor R
		vec3 ReflectionDirection = reflect(-L, N);
		// Termo especular (R . V) ^ alpha
		float Alpha = 50.0;
		SpecularReflection = pow(dot(ReflectionDirection, -ViewDirection), Alpha);
		SpecularReflection = max(0.0, SpecularReflection);
	}

	vec3 EarthColor = texture(TextureSampler, UV).rgb;
	vec3 CloudsColor = texture(CloudsTexture, UV + Time * CloudsRotationSpeed).rgb;
	vec3 SurfaceColor = EarthColor + CloudsColor;

	vec3 DiffuseReflection =  Lambertian * LightIntensity * SurfaceColor + SpecularReflection;
	OutColor = vec4(DiffuseReflection, 1.0);
}