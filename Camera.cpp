
#include "Camera.h"

#include <glm/ext.hpp>

void FlyCamera::MoveForward(float Amount) {
    Location += Direction * Amount * Speed;
}

void FlyCamera::MoveRigth(float Amount) {
    glm::vec3 Rigth = glm::normalize(glm::cross(Direction, Up)); // glm::cross retorna o produto vetorial entre Direction e Up
    Location += Rigth * Amount * Speed;
}

void FlyCamera::Look(float Yaw, float Pitch) {
    Yaw *= Sensitivity;
    Pitch *= Sensitivity;

    glm::vec3 Rigth = glm::normalize(glm::cross(Direction, Up));

    const glm::mat4 I = glm::identity<glm::mat4>();
    glm::mat4 YawRotation = glm::rotate(I, glm::radians(Yaw), Up);
    glm::mat4 PitchRotation = glm::rotate(I, glm::radians(Pitch), Rigth);

    //Up = PitchRotation * glm::vec4{ Up, 0.0f };
    Direction = YawRotation * PitchRotation * glm::vec4{ Direction, 0.0f };
}

glm::mat4 FlyCamera::GetViewProjection() const {
    glm::mat4 Projection = glm::perspective(FieldOfView, AspectRatio, Near, Far);
    return Projection * GetView();
}

glm::mat4 FlyCamera::GetView() const {
    return glm::lookAt(Location, Location + Direction, Up);
}