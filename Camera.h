#pragma once

#include <glm/glm.hpp>

class FlyCamera {
public:
    void MoveForward(float Amount);
    void MoveRigth(float Amount);
    void Look(float Yaw, float Pitch);
	glm::mat4 GetViewProjection() const;
    glm::mat4 GetView() const;

    // Paramtros de interatividade
    float Speed = 5.0f;
    float Sensitivity = 0.1f;

    // Defini��o da matriz view
    glm::vec3 Location{ 0.0f, 0.0f, 10.0f };
    glm::vec3 Direction{ 0.0f, 0.0f, -1.0f };
    glm::vec3 Up{ 0.0f, 1.0f, 0.0f };

    // Defini��o da matriz projection
    float FieldOfView = glm::radians(45.0f);
    float AspectRatio = 800 / 600;
    float Near = 0.01f;
    float Far = 1000.0f;
};