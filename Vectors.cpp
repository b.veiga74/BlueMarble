#include <iostream>

#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>

void Components() {
	std::cout << std::endl;

	glm::vec3 P{ 1, 2, 3 };
	std::cout << "X: " << P.x << " Y: " << P.y << " Z: " << P.z << std::endl;
}

void Swizzle() {
	std::cout << std::endl;
	glm::vec4 P{ 1,2,3,4 };
	glm::vec3 Q = P.xxx;
	glm::vec3 R = P.xyx;
	glm::vec4 T = P.xyzw;
	glm::vec4 Y = P.xzzw;
	std::cout << glm::to_string(Y) << std::endl;
}

void Operations() {
	glm::vec3 P0{ 10.0f, 10.0f, 0.0f };
	glm::vec3 P1{ 10.0f, 10.0f, 10.0f };
	glm::vec3 R;
	
	// Opera��es b�sicas
	R = P0 + P1;
	std::cout << "Soma ->                        " << glm::to_string(R) << std::endl;

	R = P0 - P1;
	std::cout << "Subtra��o ->                   " << glm::to_string(R) << std::endl;

	R = P0 * P1;
	std::cout << "Multiplica��o ->               " << glm::to_string(R) << std::endl;

	R = P0 / P1; // N�o pode ter 0 no P1
	std::cout << "Divisao ->                     " << glm::to_string(R) << std::endl;

	// Scala
	R = P0 * 10.0f;
	std::cout << "Escala ->                      " << glm::to_string(R) << std::endl;

	// Produto vetorial (Cross Product)
	glm::vec3 Cross = glm::cross(P0, P1);
	std::cout << "Produto vetorial de P0 e P1 -> " << glm::to_string(Cross) << std::endl;

	// Normalizar o vetor
	glm::vec3 Norm = glm::normalize(P0);
	std::cout << "P0 normalizado ->              " << glm::to_string(Norm) << std::endl;

	// Refra��o
	glm::vec3 Refract = glm::refract(P0, Norm, 1.0f);
	std::cout << "P0 refracionado ->             " << glm::to_string(Refract) << std::endl;

	// Reflex�o
	glm::vec3 Reflect = glm::reflect(P0, Norm);
	std::cout << "P0 reflexionado ->             " << glm::to_string(Reflect) << std::endl;

	// Distancia entre pontos
	float Distance = glm::distance(P0, P1);
	std::cout << "Distancia de P0 e P1 ->        " << Distance << std::endl;

	// Produto escalar (Dot Product)
	float Dot = glm::dot(P0, P1);
	std::cout << "Produto escalar de P0 e P1 ->  " << Dot << std::endl;

	// raiz de (X^2 + Y^2 + Z^2)
	float L = glm::length(P0);
	std::cout << "Tamanho do vetor ->            " << L << std::endl;
} 

int main() {
	Components();
	//Swizzle();
	Operations();
	return 0;
}