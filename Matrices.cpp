#include <iostream>
#include <iomanip>
#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>

void PrintMatrix(const glm::mat4& M) {
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
		
			std::cout
				<< std::setw(10)
				<< std::setprecision(4)
				<< std::fixed
				<< M[j][i] << " ";
		}
		std::cout << std::endl;
	}
}

void TranslationMatrix() {
	std::cout << std::endl;
	std::cout << "   =========================================" << std::endl;
	std::cout << "               Translation Matrix           " << std::endl;
	std::cout << "   =========================================" << std::endl;

	glm::mat4 I = glm::identity<glm::mat4>();
	glm::vec3 T{ 10, 10, 10 };
	glm::mat4 Translation = glm::translate(I, T);

	PrintMatrix(Translation);
	
	glm::vec4 Position{ 10, 10, 10 ,1 };
	glm::vec4 Direction{ 10, 10, 10, 0 }; 

	Position = Translation * Position;
	Direction = Translation * Direction;

	std::cout << std::endl;
	std::cout << glm::to_string(Position) << std::endl;
	std::cout << glm::to_string(Direction) << std::endl;

	PrintMatrix(Translation);
}

void ScaleMatrix() {
	std::cout << std::endl;
	std::cout << "   =========================================" << std::endl;
	std::cout << "                  Scale Matrix              " << std::endl;
	std::cout << "   =========================================" << std::endl;

	glm::mat4 I = glm::identity<glm::mat4>();
	glm::vec3 ScaleAmount{ 2, 2, 2 };
	glm::mat4 Scale = glm::scale(I, ScaleAmount);

	PrintMatrix(Scale);

	glm::vec4 Position{ 100,  100, 0 , 1 };
	glm::vec4 Direction{ 100,  100, 0 , 0 };

	Position = Scale * Position;
	Direction = Scale * Direction;

	std::cout << std::endl;
	std::cout << glm::to_string(Position) << std::endl;
	std::cout << glm::to_string(Direction) << std::endl;

}

void RotationMatrix() {
	std::cout << std::endl;
	std::cout << "   =========================================" << std::endl;
	std::cout << "                Rotation Matrix             " << std::endl;
	std::cout << "   =========================================" << std::endl;

	glm::mat4 I = glm::identity<glm::mat4>();
	constexpr float Angle = glm::radians(90.0f);
	glm::vec3 Axis{ 0, 0, 1 };
	glm::mat4 Rotation = glm::rotate(I, Angle, Axis);

	PrintMatrix(Rotation);

	glm::vec4 Position{ 100, 0, 0, 1 };
	glm::vec4 Direction{ 100, 0, 0, 0 };

	Position = Rotation * Position;
	Direction = Rotation * Direction;
	
	std::cout << std::endl;
	std::cout << glm::to_string(Position) << std::endl;
	std::cout << glm::to_string(Direction) << std::endl;
}

void ComposedMatrix() {
	std::cout << std::endl;
	std::cout << "   =========================================" << std::endl;
	std::cout << "                Composed Matrix             " << std::endl;
	std::cout << "   =========================================" << std::endl;

	// Matriz Identidade
	glm::mat4 I = glm::identity<glm::mat4>();

	// Matriz Transla��o
	glm::vec3 T{ 0, 10, 0 };
	glm::mat4 Translation = glm::translate(I, T);

	// Matriz Rota��o
	constexpr float Angle = glm::radians(45.0f);
	glm::vec3 Axis{ 0, 0, 1 };
	glm::mat4 Rotation = glm::rotate(I, Angle, Axis);

	// Matriz Escala
	glm::vec3 ScaleAmount{ 2, 2, 0 };
	glm::mat4 Scale = glm::scale(I, ScaleAmount);

	std::cout << "               Translation Matrix:" << std::endl;
	PrintMatrix(Translation);
	std::cout << std::endl;

	std::cout << "               Rotation Matrix:" << std::endl;
	PrintMatrix(Rotation);
	std::cout << std::endl;

	std::cout << "               Scale Matrix:" << std::endl;
	PrintMatrix(Scale);
	std::cout << std::endl;

	glm::vec4 Position{ 10, 10, 10 ,1 };
	glm::vec4 Direction{ 10, 10, 10, 0 };

	// Matriz Composta
	glm::mat4 Composed = Translation * Rotation * Scale; 

	std::cout << "               Composed Matrix:" << std::endl;
	PrintMatrix(Composed);
	std::cout << std::endl;

	Position = Composed * Position;
	Direction = Composed * Direction;

	std::cout << std::endl;
	std::cout << glm::to_string(Position) << std::endl;
	std::cout << glm::to_string(Direction) << std::endl;
}

void ModelViewProjection() {
	std::cout << std::endl;
	std::cout << "   =========================================" << std::endl;
	std::cout << "          Model View Projection Matrix      " << std::endl;
	std::cout << "   =========================================" << std::endl;
	
	/* Vai ser a matriz composta, que recebe transla��o rota��o e escala,
	 * vai estar "formatada" por matriz identidade. */
	glm::mat4 Model = glm::identity<glm::mat4>();
	std::cout << "Model matrix: " << std::endl;
	PrintMatrix(Model);

	/* Matriz da c�mera: 
	 * - Eye � a posi��o no espa�o
	 * - Center � a dire��o que a c�mera olha no espa�o
	 * - Up � a dire��o que a parte de cima da camera esta
	 * "olhando", serve para indicar a rota��o da c�mera. */
	glm::vec3 Eye{ 0, 0, 10 };
	glm::vec3 Center{ 0, 0, 0 };
	glm::vec3 Up{ 0, 1, 0 };
	glm::mat4 View = glm::lookAt(Eye, Center, Up);
	std::cout << "View matrix: " << std::endl;
	PrintMatrix(View);

	/* Indica as caracteristicas da vis�o da c�mera:
	 *  - FoV � o angulo de abertura da c�mera
	 *  - AspectRadio � o dimens�o da tela
	 *  - Near � a dist�ncia do plano mais pr�ximo
	 *  - Far � a dist�ncia do plano mais afastado. */
	constexpr float FoV = glm::radians(45.0f);
	const float AspectRadio = 800.f / 600.0f;
	const float Near = 0.001f;
	const float Far = 1000.0f;
	glm::mat4 Projection = glm::perspective(FoV, AspectRadio, Near, Far);
	std::cout << "Projection matrix: " << std::endl;
	PrintMatrix(Projection);

	/* Matriz transformada que ser� aplicada nos pontos das figuras
	 * geom�tricas que compoem as imagens, para encotnrar os novos
	 * pontos no espa�o definido que eles ocupam, ap�s todas as
	 * opera��es envolvidas terem sido calculadas.*/
	glm::mat4 ModelViewProjection = Projection * View * Model;
	std::cout << "Model View Projection matrix: " << std::endl;
	PrintMatrix(ModelViewProjection);

	// Aplicando o a matriz no Position
	glm::vec4 Position{ 0, 0, 0, 1 };
	Position = ModelViewProjection * Position;
	std::cout << glm::to_string(Position) << std::endl;
	/* Podemos observar que a quarta posi��o, o "Position.w", est� sendo alterada
	 * Essa vari�vel � a coordenada homogenea, o OpenGL autom�ticamente realiza a
	 * seguinte opera��o para inserir os pontos no cubo "normalizado" de tamanho 1.*/

	// Retornando o valor de w para 1;
	Position = Position / Position.w;
	std::cout << glm::to_string(Position) << std::endl;

}

int main() {
	ModelViewProjection();
	return 0;
}