
#include <iostream>
#include <cassert>
#include <array>
#include <Vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <fstream>

#include <glm/gtx/string_cast.hpp>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include "Camera.h"

// Dimens�es da tela
int Width = 800;
int Heigth = 600;

FlyCamera Camera;
bool bEnableMouseMovement = false;
glm::vec2 PreviousCursor{ 0.0, 0.0 };

// Estrutura dos v�rtices
struct Vertex {
    glm::vec3 Position;
    glm::vec3 Normal;
    glm::vec3 Color;
    glm::vec2 UV;
};

struct Triangle {
    GLuint V0;
    GLuint V1;
    GLuint V2;
};

struct DirectionalLight {
    glm::vec3 Direction;
    GLfloat Intensity;
};

void MouseButtonCallback(GLFWwindow* Window, int Button, int Action, int Modifiers) {
    std::cout << "Button:    " << Button << std::endl;
    std::cout << "Action:    " << Action << std::endl;
    std::cout << "Mofifiers: " << Modifiers << std::endl;
    if (Button == GLFW_MOUSE_BUTTON_LEFT) {
        if (Action == GLFW_PRESS) {
            glfwSetInputMode(Window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            double X, Y;
            glfwGetCursorPos(Window, &X, &Y);
            PreviousCursor = glm::vec2{ X, Y };
            bEnableMouseMovement = true;
        }
        else if (Action == GLFW_RELEASE) {
            glfwSetInputMode(Window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            bEnableMouseMovement = false;
        }
    }
}

void MouseMotionCallback(GLFWwindow* Window, double X, double Y) {
    if (bEnableMouseMovement) {
        glm::vec2 CurrentCursor{ X, Y };
        glm::vec2 DeltaCursor = CurrentCursor - PreviousCursor;
        // std::cout << "Delta cursor: " << glm::to_string(DeltaCursor) << std::endl;
        Camera.Look(-DeltaCursor.x, -DeltaCursor.y);
        PreviousCursor = CurrentCursor;
    }
}

void Resize(GLFWwindow* Window, int NewWidth, int NewHeigth) {
    Width = NewWidth;
    Heigth = NewHeigth;

    Camera.AspectRatio = static_cast<float>(Width) / Heigth;
    glViewport(0, 0, Width, Heigth);
}

void printOpenGLInfo() {
    std::cout << "OpenGL Version:  " << glGetString(GL_VERSION) << std::endl;
    std::cout << "OpenGL Vendor:   " << glGetString(GL_VENDOR) << std::endl;
    std::cout << "OpenGL Renderer: " << glGetString(GL_RENDERER) << std::endl;
    std::cout << "GLSL Version:    " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
}

std::string ReadFile(const char* FilePath) {
    std::string FileContents;
    if (std::ifstream FileStream{ FilePath, std::ios::in }) {
        // Salva em  FileContents conteudo do arquivo em FilePath
        FileContents.assign(std::istreambuf_iterator<char>(FileStream), std::istreambuf_iterator<char>());
    }
    return FileContents;
}

void GenerateSphereMesh(GLuint Resolution, std::vector<Vertex>& Vertices, std::vector<Triangle>& Indices) {
    Vertices.clear();
    Indices.clear();

    constexpr float Pi = glm::pi<float>();
    constexpr float TwoPi = glm::two_pi<float>();
    float InvResolution = 1.0f / static_cast<float>(Resolution - 1);

    for (GLuint UIndex = 0; UIndex < Resolution; UIndex++) {
        const float U = UIndex * InvResolution;
        const float Theta = glm::mix(0.0f, TwoPi, static_cast<float>(U));

        for (GLuint VIndex = 0; VIndex < Resolution; VIndex++) {
            const float V = VIndex * InvResolution;
            const float Phi = glm::mix(0.0f, Pi, static_cast<float>(V));
            
            glm::vec3 VertexPosition = {
                glm::sin(Phi) * glm::cos(Theta),
                glm::sin(Phi) * glm::sin(Theta),
                glm::cos(Phi)
            };

            Vertices.push_back(Vertex{
                VertexPosition,
                glm::normalize(VertexPosition),
                glm::vec3{ 1.0f, 1.0f, 1.0f },
                glm::vec2{U, 1.0f - V }
            });
        }
    }

    for (GLuint U = 0; U < Resolution - 1; U++) {
        for (GLuint V = 0; V < Resolution - 1; V++) {
            GLuint P0 = U + V * Resolution;
            GLuint P1 = U + 1 + V * Resolution;
            GLuint P2 = U + (V + 1) * Resolution;
            GLuint P3 = U + 1 + (V + 1) * Resolution;

            Indices.push_back(Triangle{ P3, P2, P0 });
            Indices.push_back(Triangle{ P1, P3, P0 });
        }
    }
}

GLuint LoadSphere(GLuint& NumVertices, GLuint& NumIndices) {
    std::vector<Vertex> Vertices;
    std::vector<Triangle> Triangles;
    GenerateSphereMesh(100, Vertices, Triangles);

    NumVertices = Vertices.size();
    NumIndices = Triangles.size() * 3;

    GLuint VertexBuffer;
    glGenBuffers(1, &VertexBuffer);

    glBindBuffer(GL_ARRAY_BUFFER, VertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, Vertices.size() * sizeof(Vertex), Vertices.data(), GL_STATIC_DRAW);

    GLuint ElementBuffer;
    glGenBuffers(1, &ElementBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ElementBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, NumIndices * sizeof(GLuint), Triangles.data(), GL_STATIC_DRAW);

    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);

    glBindBuffer(GL_ARRAY_BUFFER, VertexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ElementBuffer);

    // Avisa openGl qual lugar dentro do buffer de vertices est� o atributo de posi��o.
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), nullptr);
    // Avisa openGl qual lugar dentro do buffer de vertices est� o atributo de cor.
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, Normal)));
    // Avisa openGl qual lugar dentro do buffer de vertices est� o atributo da normal do v�rtice.
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_TRUE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, Color)));
    // Avisa openGl qual lugar dentro do buffer de vertices est� o atributo de coordenadas da textura.
    glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, UV)));

    glBindVertexArray(0);

    return VAO;
}

// ShaderId precisar ser um shader j� compilado
void CheckShader(GLuint ShaderId) {
    GLint Result = GL_TRUE;

    glGetShaderiv(ShaderId, GL_COMPILE_STATUS, &Result);
    
    if (Result == GL_FALSE) {

        // O OpenGL � em C, precisamos pegar o tamanho da string antes de pegar a string em s�
        GLint InfoLogLength = 0;
        glGetShaderiv(ShaderId, GL_INFO_LOG_LENGTH, &InfoLogLength);

        // Se tiver algum log � porque deu ruim
        if (InfoLogLength > 0) {
            std::string ShaderInfoLog(InfoLogLength, '\0');
            glGetShaderInfoLog(ShaderId, InfoLogLength, nullptr, &ShaderInfoLog[0]);
            std::cout << "Erro no shader" << std::endl;;
            std::cout << ShaderInfoLog << std::endl;;
            assert(false);
        }
    }
}

GLuint LoadShaders(const char* VertexShaderFile, const char* FragmentShaderFile) {
    std::string VertexShaderSource = ReadFile(VertexShaderFile);
    std::string FragmentShaderSource = ReadFile(FragmentShaderFile);

    // Confirma que os arquivos n�o est�o vazios
    assert(!VertexShaderSource.empty());
    assert(!FragmentShaderSource.empty());

    // Cria identificadores para os shaders
    GLuint VertexShaderId = glCreateShader(GL_VERTEX_SHADER);
    GLuint FragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);

    std::cout << "Compilando " << VertexShaderFile << std::endl;
    const char* VertexShadersSourcePtr = VertexShaderSource.c_str();
    glShaderSource(VertexShaderId, 1, &VertexShadersSourcePtr, nullptr);
    glCompileShader(VertexShaderId);
    // Verificar se a compila��o deu certo
    CheckShader(VertexShaderId);

    std::cout << "Compilando " << FragmentShaderFile << std::endl;
    const char* FragmentShadersSourcePtr = FragmentShaderSource.c_str();
    glShaderSource(FragmentShaderId, 1, &FragmentShadersSourcePtr, nullptr);
    glCompileShader(FragmentShaderId);
    // Verificar se a compila��o deu certo
    CheckShader(FragmentShaderId);

    std::cout << "Linkando o programa" << std::endl;
    GLuint ProgramId = glCreateProgram();
    glAttachShader(ProgramId, VertexShaderId);
    glAttachShader(ProgramId, FragmentShaderId);
    glLinkProgram(ProgramId);

    // Verificar o programa
    GLint Result = GL_TRUE;
    glGetProgramiv(ProgramId, GL_LINK_STATUS, &Result);

    if (Result == GL_FALSE) {
        GLint InfoLogLength = 0;
        glGetProgramiv(ProgramId, GL_INFO_LOG_LENGTH, &InfoLogLength);

        if (InfoLogLength > 0) {
            std::string ProgramInfoLog(InfoLogLength, '\0');
            glGetProgramInfoLog(ProgramId, InfoLogLength, nullptr, &ProgramInfoLog[0]);
            std::cout << "Erro ao linkar prorgama" << std::endl;
            std::cout << ProgramInfoLog << std::endl;
            assert(false);
        }
    }

    glDetachShader(ProgramId, VertexShaderId);
    glDetachShader(ProgramId, FragmentShaderId);

    glDeleteShader(VertexShaderId);
    glDeleteShader(FragmentShaderId);

    return ProgramId;
}

GLuint LoadTexture(const char* TextureFile) {
    std::cout << "Carregando Textura " << TextureFile << std::endl;

    stbi_set_flip_vertically_on_load(true);

    int TextureWidth = 0;
    int TextureHeight = 0;
    int NumberOfComponents = 0;
    unsigned char* TextureData = stbi_load(TextureFile, &TextureWidth, &TextureHeight, &NumberOfComponents, 3);

    assert(TextureData);

    // Gerar identificador da textura
    GLuint TextureId;
    glGenTextures(1, &TextureId);

    // Habilitar textura para ser modificada
    glBindTexture(GL_TEXTURE_2D, TextureId);

    GLint Level = 0;
    GLint Border = 0;

    /* Copiar textura para mem�ria de v�deo(GPU), a partir daqui vamos usar fun��es do OpenGL
     * para modificar a textura mas sem precisar passar a texura como par�metro, ela j� vai */
    glTexImage2D(GL_TEXTURE_2D,         // Tipo da textura
                 Level,                 // Level of detail, � a quantidade de imagens que o mipmap vai gerar para a renderiza��o
                 GL_RGB,                // Tipo de colora��o da textura
                 TextureWidth,          // Largura
                 TextureHeight,         // Altura
                 Border,                // Borda (?)
                 GL_RGB,                // Formato de p�xel (Normalmente � igual ao tipo de colora��o)
                 GL_UNSIGNED_BYTE,      // Tipo de dado que a textura usa
                 TextureData);          // Pixels da textura

    // Filtros de magnifica��o e minifica��o
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    // Configurar Texture Wraping
    glTexParameteri(GL_TEXTURE_2D, GL_CLAMP_TO_EDGE, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_MIRRORED_REPEAT, GL_REPEAT);

    // Gerar mipmap a partir da textura
    glGenerateMipmap(GL_TEXTURE_2D);

    /* Depois de realizar as opera��es com a textura na mem�ria gr�fica,
     * fazemos o unbind para liberar recursos da GPU */
    glBindTexture(GL_TEXTURE_2D, 0);

    stbi_image_free(TextureData);
    return TextureId;
}

int main() {
    // Inicializar a biblioteca GLFW
    assert(glfwInit() == GLFW_TRUE);

    // Criar janela
    GLFWwindow* Window = glfwCreateWindow(Width, Heigth, "Blue Marble", nullptr, nullptr);
    assert(Window);

    // Caddastrar as callbacks no GLFW
    glfwSetMouseButtonCallback(Window, MouseButtonCallback);
    glfwSetCursorPosCallback(Window, MouseMotionCallback);
    glfwSetFramebufferSizeCallback(Window, Resize);

    glfwMakeContextCurrent(Window);

    // Habilita o VSync
    glfwSwapInterval(1);

    // Inicializa lib GLEW
    assert(glewInit() == GLEW_OK);

    // Verificar vers�o do OpenGL
    printOpenGLInfo();

    Resize(Window, Width, Heigth);
    GLuint ProgramId = LoadShaders("shaders/triangle_vert.glsl", "shaders/triangle_frag.glsl");

    GLuint TextureId = LoadTexture("textures/earth_2k.jpg");
    GLuint CloudTextureId = LoadTexture("textures/earth_clouds_2k.jpg");

    GLuint SphereNumVertices = 0;
    GLuint SphereNumIndices  = 0;
    GLuint SphereVAO = LoadSphere(SphereNumVertices, SphereNumIndices);

    // Model
    glm::mat4 I = glm::identity<glm::mat4>();
    glm::mat4 ModelMatrix = glm::rotate(I, glm::radians(-90.0f), glm::vec3{ 1,0,0 });

    // Define cor de fundo
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    // Guarda o tempo do frame anterior
    double PreviousTime = glfwGetTime();

    // Habilita backface culling
    //glEnable(GL_CULL_FACE);
    //glCullFace(GL_BACK);

    // Habilitar o Z-Buffer
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    // Criar fonte de luz direcional;
    DirectionalLight Light;
    Light.Direction = glm::vec3{ 0.0f, 0.0f, -1.0f };
    Light.Intensity = 1.0f;

    // Event looping
    while (!glfwWindowShouldClose(Window)) {
        double CurrentTime = glfwGetTime();
        double DeltaTime   = CurrentTime - PreviousTime;
        if (DeltaTime > 0.0) {
            PreviousTime = CurrentTime;
        }

        // Limpa o buffer e pinta o definido em glClearColor
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Ativar o programa de shader
        glUseProgram(ProgramId);

        // Model View Projection 
        glm::mat4 NormalMatrix = glm::inverse(glm::transpose(Camera.GetView() * ModelMatrix));
        glm::mat4 ViewProjection = Camera.GetViewProjection();
        glm::mat4 ModelViewProjection = ViewProjection * ModelMatrix;

        GLint TimeLoc = glGetUniformLocation(ProgramId, "Time");
        glUniform1f(TimeLoc, CurrentTime);

        GLint ModelViewProjectionLoc = glGetUniformLocation(ProgramId, "ModelViewProjection");
        glUniformMatrix4fv(ModelViewProjectionLoc, 1, GL_FALSE, glm::value_ptr(ModelViewProjection));

        GLint NormalMatrixLoc = glGetUniformLocation(ProgramId, "NormalMatrix");
        glUniformMatrix4fv(NormalMatrixLoc, 1, GL_FALSE, glm::value_ptr(NormalMatrix));

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, TextureId);

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, CloudTextureId);

        GLint TextureSamplerLoc = glGetUniformLocation(ProgramId, "TextureSampler");
        glUniform1i(TextureSamplerLoc, 0);

        GLint CloudTextureLoc = glGetUniformLocation(ProgramId, "CloudsTexture");
        glUniform1i(CloudTextureLoc, 1);

        GLint LightDirectionLoc = glGetUniformLocation(ProgramId, "LightDirection");
        glUniform3fv(LightDirectionLoc, 1, glm::value_ptr(Camera.GetView() * glm::vec4{ Light.Direction, 0.0f }));

        GLint LightIntensityLoc = glGetUniformLocation(ProgramId, "LightIntensity");
        glUniform1f(LightIntensityLoc, Light.Intensity);

        glBindVertexArray(SphereVAO);

        // Pede pro OpenGL por favor desenha esse triangulo
        glPointSize(1.0f);
        glLineWidth(10.0f);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        //glDrawArrays(GL_TRIANGLES, 0, Quad.size());
        //glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
        //glDrawArrays(GL_TRIANGLES, 0, SphereNumVertices * 3);
        glDrawElements(GL_TRIANGLES, SphereNumIndices, GL_UNSIGNED_INT, nullptr);

        glBindVertexArray(0);

        // Desabilita o programa (shader) ativo
        glUseProgram(0);

        // Captura todos os eventos (input)
        glfwPollEvents();

        // Desenha na tela
        glfwSwapBuffers(Window);

        // Processar inputs do teclado
        if (glfwGetKey(Window, GLFW_KEY_W) == GLFW_PRESS) {
            Camera.MoveForward(1.0f * DeltaTime);
        }
        if (glfwGetKey(Window, GLFW_KEY_S) == GLFW_PRESS) {
            Camera.MoveForward(-1.0f * DeltaTime);
        }
        if (glfwGetKey(Window, GLFW_KEY_D) == GLFW_PRESS) {
            Camera.MoveRigth(1.0f * DeltaTime);
        }
        if (glfwGetKey(Window, GLFW_KEY_A) == GLFW_PRESS) {
            Camera.MoveRigth(-1.0f * DeltaTime);
        }
    }

    // Desaloca vertexBuffer
    glDeleteVertexArrays(1, &SphereVAO);

    // Encerra biblioteca GLFW
    glfwTerminate();
    return 0;
}